# Nagłówek

pierwszy paragraf

drugi paragraf

trzeci paragraf

~~przekreślenie~~ **pogrubienie** *kursywa*

>cytat

1. lista
    1. lista w liście
2. lista

- lista z kropką na początku
    - lista w liście

```py
def suma(a,b):
    wynik=a+b
    return wynik
print(suma(10,29))
print(suma(2,4))
```
Przykładowy fragment `print("Hello")`

![zdjecie.jpeg](zdjecie.jpeg)
